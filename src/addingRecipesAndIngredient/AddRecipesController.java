package addingRecipesAndIngredient;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.List;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;

public class AddRecipesController {

	@FXML
	ListView<GridPaneForIngredient> listViewIngredientListForRecipe;

	@FXML
	ComboBox<String> comboBoxForHowManyPortions;

	@FXML
	ImageView imageViewPictureOfMeal;

	@FXML
	GridPane gridPaneForPicture;

	@FXML
	Button buttonSaveNewRecipe;

	@FXML
	Button buttonAddIngredientsToRecipe;

	ObservableList<String> portionsItems;

	private File filePictureOfMeal;
	private FileInputStream fileInputStream;
	private Image imagePictureOfMeal;

	@FXML
	void buttonAddIngredientsToRecipeAction(ActionEvent e) {
		AddIngredientsToRecipeWindow.getInstance().getStage().show();
	}

	@FXML
	private void initialize() {
		listViewIngredientListForRecipe.setItems(AddRecipesModel.getInstance().getObservableListOfSelectedIngredients());

		System.out.println("Initialize controller AddRecipe");
		portionsItems = FXCollections.observableArrayList();
		portionsItems.addAll("1", "2", "3","4","5","6","7","8","9","10");
		comboBoxForHowManyPortions.setItems(portionsItems);

		imageViewPictureOfMeal.setPreserveRatio(true);

		imageViewPictureOfMeal.fitWidthProperty().bind(gridPaneForPicture.widthProperty());
		imageViewPictureOfMeal.fitHeightProperty().bind(gridPaneForPicture.heightProperty());


//		WZ�R
//		iv = new ImageView();
//		iv.setPreserveRatio(true);
//
//		iv.fitWidthProperty().bind(stage.widthProperty());
//		iv.fitHeightProperty().bind(stage.heightProperty());

	}



	public AddRecipesController() { //Klasa Controller przy projektach ze scenebuilderem wymaga do swojego dzia�ania konstruktora bezargumentowego
		super();
		filePictureOfMeal = new File("jpeg_na_imageView_przepisu.png");
		try {
			fileInputStream = new FileInputStream(filePictureOfMeal);
		} catch (FileNotFoundException e) {	e.printStackTrace();	}

//		imagePictureOfMeal = new Image(fileInputStream);  //  prawdopodobnie bedzie to do usuniecia bo robie to w metodize drag droped
		//imageViewPictureOfMeal = new ImageView();


//		imagePictureOfMeal = new Image("jpeg_na_imageView_przepisu.png");
//		imageViewPictureOfMeal = new ImageView();
//		imageViewPictureOfMeal.setImage(imagePictureOfMeal);

	}

	@FXML
	void buttonSaveNewRecipeAction(ActionEvent e) {

	}

/*	WZ�R
	void imageDragOver(DragEvent event) {
		Dragboard board = event.getDragboard();
		if (board.hasFiles()) {
			event.acceptTransferModes(TransferMode.ANY);
		}
	}
*/



	@FXML
	void onDragOverImageViewPictureOfMeal (DragEvent event) {
			//System.out.println("Over DragBoard");		//spamujacy sysout :P
		Dragboard dragboard = event.getDragboard();

		if (dragboard.hasFiles()) {
		event.acceptTransferModes(TransferMode.ANY); // podpowiada komunikat przenie�
		}	}

	@FXML
	void onDragDropedImageViewPictureOfMeal(DragEvent event) {

		System.out.println("W�a�nie upuszczono zdjecie przepisu");
		try {

		Dragboard dragboard = event.getDragboard();
		List<File> files = dragboard.getFiles();

		fileInputStream = new FileInputStream(files.get(0));
		imagePictureOfMeal = new Image(fileInputStream);
		imageViewPictureOfMeal.setImage(imagePictureOfMeal);


		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

	}

/*	@FXML // wz�r z przykladu z mackiem
	private void dragDrop(DragEvent event) {
		try {

		Dragboard dragboard = event.getDragboard();
		List<File> files = dragboard.getFiles();

			FileInputStream stream = new FileInputStream(files.get(0));

			Image image = new Image(stream);
			imageView.setImage(image);


		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

	}*/

//	void imageDropped(DragEvent event) {
//		try {
//			Dragboard board = event.getDragboard();
//			List<File> files = board.getFiles();
//			FileInputStream stream;
//			stream = new FileInputStream(files.get(0));
//			Image image = new Image(stream);
//			setImage(image);
//		} catch (FileNotFoundException e) {
//			e.printStackTrace();
//		}
//	}


	/*
	 * Each row in a ListView should be 24px tall.  Also, we have to add an extra
	 * two px to account for the borders of the ListView.
	 */
//	final int ROW_HEIGHT = 24;
//	final ObservableList items = FXCollections.observableArrayList("1", "2", "3");
//	final ListView list = new ListView(items);
//
//	// This sets the initial height of the ListView:
//	list.setPrefHeight(items().size() * ROW_HEIGHT + 2);

	/*
	Now we have to add a ListChangeListener to the ObservableList. When the list changes,
	we simply change the set height of the ListView to match the new number of rows.
	If we know that we are never going to add or remove items from the ObservableList
	that is backing the ListView, then we can exclude the listener. Again, the height is
	the number of rows times the height per row plus two extra pixels for the borders:
	*/

	/*
	 * This listener will resize the ListView when items are added or removed
	 * from the ObservableList that is backing the ListView:
	 */
//	items.addListener(new ListChangeListener() {
//	    @Override
//	    public void onChanger(ListChangeListener.Change change) {
//	        list.setPrefHeight(items.size() * ROW_HEIGHT + 2);
//	    }
//	});
}
