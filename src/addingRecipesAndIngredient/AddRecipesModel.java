package addingRecipesAndIngredient;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class AddRecipesModel {	//Singleton

	ObservableList<GridPaneForIngredient> observableListOfSelectedIngredients;

	private static AddRecipesModel instance;

	public static AddRecipesModel getInstance() {

		if(instance == null)
			instance = new AddRecipesModel();
		return instance;
	}

	private AddRecipesModel() {

		observableListOfSelectedIngredients = FXCollections.observableArrayList();

	}



	public ObservableList<GridPaneForIngredient> getObservableListOfSelectedIngredients() {
		return observableListOfSelectedIngredients;
	}

	public void setObservableListOfSelectedIngredients(
			ObservableList<GridPaneForIngredient> observableListOfSelectedIngredients) {
		this.observableListOfSelectedIngredients = observableListOfSelectedIngredients;
	}

	public static void setInstance(AddRecipesModel instance) {
		AddRecipesModel.instance = instance;
	}



}
