package addingRecipesAndIngredient;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import model.ingredientsAndRecipes.Measures;

public class MeasureTypeOfNewIngredientController {

	@FXML
	Label labelNameOfNewIngredient;

	@FXML
	Button buttonConfirmAdding;

	@FXML
	Button buttonCancelAdding;

	@FXML
	ComboBox<Measures> comboBoxMeasureTypes;

	ObservableList<Measures> observableListMeasures;

	@FXML
	private void initialize() {

		labelNameOfNewIngredient.setText(AddIngredientComunicationModel.getInstance().getNameOfAddingIngredient());

		observableListMeasures = FXCollections.observableArrayList();

		for (Measures measure : Measures.values()) {
			observableListMeasures.add(measure);
		}

		comboBoxMeasureTypes.setItems(observableListMeasures);

		// to zawiesza program!? dlaczego????
//		MeasureTypeOfNewIngredientWindow.getInstance().getStage().setOnShowing(
//				(e) -> labelNameOfNewIngredient.setText(AddIngredientComunicationModel.getInstance().getNameOfAddingIngredient()));


	}


	@FXML
	private void buttonConfirmAddingAction(ActionEvent event) {
		if(comboBoxMeasureTypes.getValue() == null)
			return;
		FinalIngredientAdditionToTheDataBase.add(labelNameOfNewIngredient.getText(), comboBoxMeasureTypes.getValue());
		System.out.println("Confirm");
		MeasureTypeOfNewIngredientWindow.getInstance().getStage().hide();
		MeasureTypeOfNewIngredientWindow.setInstance(null);
	}

	@FXML
	private void buttonCancelAddingAction(ActionEvent event) {
		MeasureTypeOfNewIngredientWindow.getInstance().getStage().hide();
		MeasureTypeOfNewIngredientWindow.setInstance(null);
	}

	// GETTERY & SETTERY*************************************************

	public Label getLabelNameOfNewIngredient() {
		return labelNameOfNewIngredient;
	}

	public void setLabelNameOfNewIngredient(Label labelNameOfNewIngredient) {
		this.labelNameOfNewIngredient = labelNameOfNewIngredient;
	}

	public Button getButtonConfirmAdding() {
		return buttonConfirmAdding;
	}

	public void setButtonConfirmAdding(Button buttonConfirmAdding) {
		this.buttonConfirmAdding = buttonConfirmAdding;
	}

	public Button getButtonCancelAdding() {
		return buttonCancelAdding;
	}

	public void setButtonCancelAdding(Button buttonCancelAdding) {
		this.buttonCancelAdding = buttonCancelAdding;
	}

	public ComboBox<Measures> getComboBoxMeasureTypes() {
		return comboBoxMeasureTypes;
	}

	public void setComboBoxMeasureTypes(ComboBox<Measures> comboBoxMeasureTypes) {
		this.comboBoxMeasureTypes = comboBoxMeasureTypes;
	}

	public ObservableList<Measures> getObservableListMeasures() {
		return observableListMeasures;
	}

	public void setObservableListMeasures(ObservableList<Measures> observableListMeasures) {
		this.observableListMeasures = observableListMeasures;
	}

}