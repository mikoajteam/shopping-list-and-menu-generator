package addingRecipesAndIngredient;

import java.io.IOException;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class AddIngredientWindow {

	private static AddIngredientWindow instance;

	private Stage stage;
	private Scene scene;
	private Parent root;


	public static AddIngredientWindow getInstance() {

		if (instance == null) {
			instance = new AddIngredientWindow();
		}
		return instance;
	}

	private AddIngredientWindow() {
		super();

		stage = new Stage();
		try {
			root = FXMLLoader.load(getClass().getResource("AddIngredientWindow.fxml"));
		} catch (IOException e) {		}

		scene = new Scene(root, 600, 300);
		stage.setScene(scene);

System.out.println("Wykonuje konstruktor Window");
	}


	@FXML
	private void initialize() {		//ta metoda sie nie wykonuje. motoda initialize() wykonuje sie
		// w klasie kt�ra jest wskazana w pliku fxml jako kontroler.

		System.out.println("initialize add window");	// ten sysout sie nigdy nie pojawi
	}


//*********************		GETTERY & SETTERY
	public Stage getStage() {
		return stage;
	}

	public void setStage(Stage stage) {
		this.stage = stage;
	}

	public Scene getScene() {
		return scene;
	}

	public void setScene(Scene scene) {
		this.scene = scene;
	}

	public Parent getRoot() {
		return root;
	}

	public void setRoot(Parent root) {
		this.root = root;
	}

	public static void setInstance(AddIngredientWindow instance) {
		AddIngredientWindow.instance = instance;
	}




}
