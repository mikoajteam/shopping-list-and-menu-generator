package addingRecipesAndIngredient;

import javafx.scene.control.Label;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import model.ingredientsAndRecipes.Measures;

public class GridPaneForIngredient extends GridPane {

	private ColumnConstraints columnName;
	private ColumnConstraints columnAmountOfIngredient;
	private ColumnConstraints columnMeasureType;

	private Label labelName;
	private Label labelAmountOfIngredient;
	private Label labelMeasureType;

	private String stringMeasureType;

	public GridPaneForIngredient(String stringName, String stringAmountOfIngredient, String stringMeasureType) {
		super();
		columnName = new ColumnConstraints();
		columnName.setPercentWidth(50);
		columnAmountOfIngredient = new ColumnConstraints();
		columnAmountOfIngredient.setPercentWidth(20);
		columnMeasureType = new ColumnConstraints();
		columnMeasureType.setPercentWidth(30);

		this.stringMeasureType = stringMeasureType;

		this.getColumnConstraints().addAll(columnName, columnAmountOfIngredient, columnMeasureType);

		String measureTypeToView = null;
		if(stringMeasureType.equals(Measures.WEIGHT_IN_GRAMS.toString()))
			measureTypeToView = "gram";
		else if (stringMeasureType.equals(Measures.ITEMS.toString()))
			measureTypeToView = "sztuk";
		else if (stringMeasureType.equals(Measures.CAPACITY_IN_MILLILITERS.toString()))
			measureTypeToView = "mililitrów";

		labelName = new Label(stringName);
		labelAmountOfIngredient = new Label(stringAmountOfIngredient);
		labelMeasureType = new Label(measureTypeToView);

		this.add(labelName, 0, 0);
		this.add(labelAmountOfIngredient, 1, 0);
		this.add(labelMeasureType, 2, 0);


	}

	public ColumnConstraints getColumnName() {
		return columnName;
	}

	public void setColumnName(ColumnConstraints columnName) {
		this.columnName = columnName;
	}

	public ColumnConstraints getColumnAmountOfIngredient() {
		return columnAmountOfIngredient;
	}

	public void setColumnAmountOfIngredient(ColumnConstraints columnAmountOfIngredient) {
		this.columnAmountOfIngredient = columnAmountOfIngredient;
	}

	public ColumnConstraints getColumnMeasureType() {
		return columnMeasureType;
	}

	public void setColumnMeasureType(ColumnConstraints columnMeasureType) {
		this.columnMeasureType = columnMeasureType;
	}

	public Label getLabelName() {
		return labelName;
	}

	public void setLabelName(Label labelName) {
		this.labelName = labelName;
	}

	public Label getLabelAmountOfIngredient() {
		return labelAmountOfIngredient;
	}

	public void setLabelAmountOfIngredient(Label labelAmountOfIngredient) {
		this.labelAmountOfIngredient = labelAmountOfIngredient;
	}

	public Label getLabelMeasureType() {
		return labelMeasureType;
	}

	public void setLabelMeasureType(Label labelMeasureType) {
		this.labelMeasureType = labelMeasureType;
	}

	public String getStringMeasureType() {
		return stringMeasureType;
	}

	public void setStringMeasureType(String stringMeasureType) {
		this.stringMeasureType = stringMeasureType;
	}



}

//kolumn10 = new ColumnConstraints();
//kolumn10.setPercentWidth(10);
//kolumn35 = new ColumnConstraints();
//kolumn35.setPercentWidth(35);
//kolumn10.setHalignment(HPos.CENTER);
//kolumn35.setHalignment(HPos.CENTER);
//root.getColumnConstraints().addAll(kolumn10, kolumn35, kolumn10, kolumn35, kolumn10);
//
//row10 = new RowConstraints();
//row35 = new RowConstraints();
//row10.setPercentHeight(10);
//row35.setPercentHeight(35);
//row10.setValignment(VPos.CENTER);
//row35.setValignment(VPos.CENTER);
//root.getRowConstraints().addAll(row10, row35, row10, row35, row10);