package addingRecipesAndIngredient;

import java.io.IOException;

import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

public class MeasureTypeOfNewIngredientWindow {

	private Stage stage;
	private Scene scene;
	private Parent root;

	private static MeasureTypeOfNewIngredientWindow instance;

	public static MeasureTypeOfNewIngredientWindow getInstance() {

		if(instance == null)
			instance = new MeasureTypeOfNewIngredientWindow();
		return instance;
	}

	private MeasureTypeOfNewIngredientWindow() {
		super();

		stage = new Stage();
		try {
			root = FXMLLoader.load(getClass().getResource("MeasureTypeOfNewIngredient.fxml"));
		} catch (IOException e) {	e.printStackTrace();	}

		scene = new Scene(root, 600, 300);
		stage.setScene(scene);

		stage.setOnCloseRequest((e) -> MeasureTypeOfNewIngredientWindow.setInstance(null));

		// dobre metodki ale ich teraz jednak nie u�yje
		//stage.setOnShowing((e) -> MeasureTypeOfNewIngredientController.;
		//stage.setOnShown(value);

System.out.println("SINGLETON MEASURETYPE WINDOW DONE");

	}

	// GETTERS & SETTERS


	public Stage getStage() {
		return stage;
	}

	public void setStage(Stage stage) {
		this.stage = stage;
	}

	public Scene getScene() {
		return scene;
	}

	public void setScene(Scene scene) {
		this.scene = scene;
	}

	public Parent getRoot() {
		return root;
	}

	public void setRoot(Parent root) {
		this.root = root;
	}

	public static void setInstance(MeasureTypeOfNewIngredientWindow instance) {
		MeasureTypeOfNewIngredientWindow.instance = instance;
	}





}
