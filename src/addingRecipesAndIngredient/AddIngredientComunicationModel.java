package addingRecipesAndIngredient;

import model.ingredientsAndRecipes.IngredientsList;

public class AddIngredientComunicationModel {

	private String nameOfAddingIngredient;

	private static AddIngredientComunicationModel instance;


	public static AddIngredientComunicationModel getInstance() {

		if(instance == null)
			instance = new AddIngredientComunicationModel();
		return instance;
	}

	private AddIngredientComunicationModel() {} // konstruktor


//********************** GETTERY & SETTERY

	public String getNameOfAddingIngredient() {
		return nameOfAddingIngredient;
	}

	public void setNameOfAddingIngredient(String nameOfAddingIngredient) {
		this.nameOfAddingIngredient = nameOfAddingIngredient;
	}

	public static void setInstance(AddIngredientComunicationModel instance) {
		AddIngredientComunicationModel.instance = instance;
	}


}
