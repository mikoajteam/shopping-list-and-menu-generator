package addingRecipesAndIngredient;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.transformation.FilteredList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import model.ingredientsAndRecipes.Ingredient;
import model.ingredientsAndRecipes.IngredientsList;

public class AddIngredientController {

	@FXML
	TextField textFieldNewIngredient;

	@FXML
	Button buttonAdd;

	@FXML
	Label labelIsAddingAllowed;

	@FXML
	ListView<String> ingredientListView;

//    private boolean isAddingAllowed;

	@FXML
	public void buttonAddAction() {

		if(AddIngredientModel.getInstance().isAddingAllowed()){
			System.out.println("Dziala!!!!!!!!!! Dodajemy skladnik");

			AddIngredientComunicationModel.getInstance().setNameOfAddingIngredient(textFieldNewIngredient.getText());
			MeasureTypeOfNewIngredientWindow.getInstance().getStage().show();
			textFieldNewIngredient.setText("");

		}
		else
			System.out.println( "Taki skladnik ju� jest, nie dodajemy go 2 raz");

	}


	//ObservableList<Ingredient> ingredientsObservableList;

//	ObservableList<String> ingredientsObservableList;
//    FilteredList<String> filteredIngredientList;


	@FXML
	private void initialize() {
		System.out.println("wykonuje initialize controller");

		AddIngredientModel.getInstance().setIngredientsObservableList(FXCollections.observableArrayList());
//		ingredientsObservableList = FXCollections.observableArrayList();

	//	IngredientList.getInstance().getIngredientList().
	//	ArrayList<String> listNameIngredients =

		for (Ingredient ingredient : IngredientsList.getInstance().getIngredientsList()) {

			AddIngredientModel.getInstance().getIngredientsObservableList().add(ingredient.getName());
		}

		AddIngredientModel.getInstance().setFilteredIngredientList(new FilteredList<>(AddIngredientModel.getInstance().getIngredientsObservableList(), p -> true));


		ingredientListView.setItems(AddIngredientModel.getInstance().getFilteredIngredientList());

		textFieldNewIngredient.textProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				String newValueToCheck = newValue.toLowerCase();
				String oldValueToCheck = oldValue.toLowerCase();
	            System.out.println(oldValue + "  -->  " + newValue);

	            if(textFieldNewIngredient != null
	            		&& !textFieldNewIngredient.getText().equals("")
	            		&& !AddIngredientModel.getInstance().getIngredientsObservableList().contains(newValueToCheck)
	            		&& !newValueToCheck.contains(",")
	            		) {
	            	AddIngredientModel.getInstance().setAddingAllowed(true);
	            	labelIsAddingAllowed.setVisible(true);
	            	}
	            else {
	            	AddIngredientModel.getInstance().setAddingAllowed(false);
	            	labelIsAddingAllowed.setVisible(false);
	            }



	            AddIngredientModel.getInstance().getFilteredIngredientList().setPredicate(e -> {
					String s1 = newValue.toLowerCase(); // nie zale�nie czy zostana wprowadzone duze czy male litery zostana przerobione na male
					String s2 = e.toLowerCase();

	                if (s2.contains(s1)) {
	                  return true; //zostanie na li�cie
	                } else {
	                  return false; //wyleci z listy
	                }
				});

			}
		});

	}



	public AddIngredientController() {
		super();
		System.out.println("wykonuje konstruktor controller");


	}



}
