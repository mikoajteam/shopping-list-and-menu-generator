package addingRecipesAndIngredient;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import model.ingredientsAndRecipes.Ingredient;
import model.ingredientsAndRecipes.IngredientsList;

public class AddIngredientsToRecipeController {

	@FXML
	ListView<String> listViewAllIngredients;

	@FXML
	ListView<GridPaneForIngredient> listViewSelectedIngredientsToRecipe;

	@FXML
	TextField textFieldFindIngredient;

	@FXML
	TextField textFieldAmountOfIngredient;

	@FXML
	Label labelInsertedIngredientName;

	@FXML
	Label labelMeasureType;

	@FXML
	Button buttonAddSelectedIngredient;

	@FXML
	Button buttonDeleteIngredientFromRecipe;

	@FXML
	Button buttonReady;


	List<Ingredient> ingredientsOnNewRecipe;

	@FXML
	void buttonReadyAction(ActionEvent event) {
		System.out.println("Button ready :)");

	//	listViewAllIngredients.getItems().clear();	// dlaczego kompilator sie przy tym wywalil??

		AddIngredientsToRecipeModel.getInstance().getObservableListSelectedIngredientsToRecipe().stream()
			.forEach(AddRecipesModel.getInstance().getObservableListOfSelectedIngredients()::add);

		AddIngredientsToRecipeModel.getInstance().getObservableListSelectedIngredientsToRecipe().clear();

		AddIngredientsToRecipeWindow.getInstance().getStage().hide();

		AddIngredientsToRecipeModel.setInstance(null);
		AddIngredientsToRecipeWindow.setInstance(null);



	}

	@FXML
	void buttonAddSelectedIngredientAction(ActionEvent event) {
		double amountOfIngredient = 0;

		if(
		labelInsertedIngredientName.getText().equals("Wprowadzany sk�adnik") ||
		labelMeasureType.getText().equals("Miara sk�adnika") ||
		textFieldAmountOfIngredient.getText().equals("") ||
		textFieldAmountOfIngredient.getText().equals(null) ||
		textFieldAmountOfIngredient.getText().contains(",")
		) {
			System.out.println("Najpierw wprowad� poprawne dane");
			return;
			}
//????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????
		// dodajemy tylko je�li taki skaladnik ju� wcze�niej nie zostal dodany
		if(
				AddIngredientsToRecipeModel.getInstance().getObservableListSelectedIngredientsToRecipe().stream().anyMatch(e -> e.getLabelName().getText()
					.equals(labelInsertedIngredientName.getText()))
				//stream(). Zwr�c mi czy kt�regokolwiek elementu nazwa r�wna sie wprowadzonej nazwie. je�li kt�rykolwiej jest r�wny zostaje zwr�cone true;
				// program wchodzi w blok z instrukcji� return; i tym samym ko�czy metode i nie dochodzi do dodania istniej�cego ju� skladnika drugi raz do listy :)


			)
		{
			System.out.println("Wprowadzony skladnik ju� widnieje na liscie, nie dodajemy powt�rze�");
			return;
		}

			try {
				amountOfIngredient = Double.parseDouble(textFieldAmountOfIngredient.getText().toString());
				if (amountOfIngredient > 1000000 || amountOfIngredient <= 0) {
					System.out.println("Podana warto�c jest zbyt du�a lub mniejsza r�wna zero");
					return;
					}

			} catch (Exception e) {
				System.out.println("Wprowadzono z�� warto�c ilo�ci skladnika");
				return;
			}

			System.out.println("Dane wprowadzone poprawne");


			GridPaneForIngredient newIngredient = new GridPaneForIngredient(
					labelInsertedIngredientName.getText(), Double.toString(amountOfIngredient), labelMeasureType.getText());

			newIngredient.setMaxWidth(listViewSelectedIngredientsToRecipe.getWidth());

			AddIngredientsToRecipeModel.getInstance().getObservableListSelectedIngredientsToRecipe().add(newIngredient);

			// czyszczenie kontrolek
			listViewAllIngredients.getSelectionModel().clearSelection(); // dzieki listenerowi oba labele wracaj� do defaulta dzieki skasowaniu zaznaczonego elementu z listy wszystkich skladnik�w
			textFieldFindIngredient.setText("");

	} // koniec buttonAddSelectedIngredientAction()

	@FXML
	void buttonDeleteIngredientFromRecipeAction(ActionEvent event) {
		System.out.println("button usu�");
		if (listViewSelectedIngredientsToRecipe.getSelectionModel().getSelectedItem() != null)
		{
			System.out.println("element jest zaznaczony wiec usuwamy");
		AddIngredientsToRecipeModel.getInstance().getObservableListSelectedIngredientsToRecipe()
		.remove(listViewSelectedIngredientsToRecipe.getSelectionModel().getSelectedIndex());
		listViewSelectedIngredientsToRecipe.getSelectionModel().clearSelection();
		}


	}

	@FXML
	private void initialize() {
System.out.println("Initialize Skladniki do przepisu Controller");

	ingredientsOnNewRecipe = new ArrayList<>();

	AddRecipesModel.getInstance().getObservableListOfSelectedIngredients().stream()
		.forEach(AddIngredientsToRecipeModel.getInstance().getObservableListSelectedIngredientsToRecipe()::add);

	AddRecipesModel.getInstance().getObservableListOfSelectedIngredients().clear();
	//AddIngredientsToRecipeModel.getInstance().getObservableListSelectedIngredientsToRecipe()

	listViewSelectedIngredientsToRecipe.setItems(AddIngredientsToRecipeModel.getInstance().getObservableListSelectedIngredientsToRecipe());

	listViewAllIngredients.setItems(AddIngredientsToRecipeModel.getInstance().getFilteredListAllIngredients());

	textFieldFindIngredient.textProperty().addListener(new ChangeListener<String>() {

		@Override
		public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {

			String searchingString = newValue.toLowerCase();

			AddIngredientsToRecipeModel.getInstance().getFilteredListAllIngredients().setPredicate(p -> {
				if(p.contains(searchingString))
					return true; // Je�li skladnik na li�cie zawiera wprowadzony ci�g znak�w zostanie on na li�cie
				else
					return false; // je�li nie zawiera takiego ci�gu znak�w to wyleci z listy
			});
		}
	}); //koniec - textFieldFindIngredient.listener


	listViewAllIngredients.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {

		@Override
		public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {


				if(observable.getValue() != null){

				String name = observable.getValue().toString().toLowerCase();
				String measure = null;

				//Jak by to mo�na zapisac za pomoca strimsow??
				for (Ingredient ingredient : IngredientsList.getInstance().getIngredientsList()) {
					if (ingredient.getName().equals(name)) {
						measure = ingredient.getMeasureType().toString();
					}
				}

					labelInsertedIngredientName.setText(name);
				labelMeasureType.setText(measure);
			}

					else {
				labelInsertedIngredientName.setText("Wprowadzany sk�adnik");
				labelMeasureType.setText("Miara sk�adnika");
				textFieldAmountOfIngredient.setText("");
					}
		}
	});


	} // koniec initialize

	public AddIngredientsToRecipeController() {
		System.out.println("Konstruktor Skladniki do przepisu Controller");

	}

}



//
//	/*
//	 * Each row in a ListView should be 24px tall.  Also, we have to add an extra
//	 * two px to account for the borders of the ListView.
//	 */
//	final int ROW_HEIGHT = 24;
//	final ObservableList items = FXCollections.observableArrayList("1", "2", "3");
//	final ListView list = new ListView(items);
//
//	// This sets the initial height of the ListView:
//	list.setPrefHeight(items().size() * ROW_HEIGHT + 2);
//
//
//	/*
//	 * This listener will resize the ListView when items are added or removed
//	 * from the ObservableList that is backing the ListView:
//	 */
//	items.addListener(new ListChangeListener() {
//	    @Override
//	    public void onChanger(ListChangeListener.Change change) {
//	        list.setPrefHeight(items.size() * ROW_HEIGHT + 2);
//	    }
//	});
//

