package addingRecipesAndIngredient;

import java.util.stream.Collectors;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import model.ingredientsAndRecipes.Ingredient;
import model.ingredientsAndRecipes.IngredientsList;

public class AddIngredientsToRecipeModel {

	FilteredList<String> filteredListAllIngredients;
	ObservableList<String> observableListAllIngredients;

	ObservableList<GridPaneForIngredient> observableListSelectedIngredientsToRecipe;

	private static AddIngredientsToRecipeModel instance;

	public static AddIngredientsToRecipeModel getInstance() {

		if(instance == null)
			instance = new AddIngredientsToRecipeModel();
		return instance;
	}

	private AddIngredientsToRecipeModel() {

		observableListAllIngredients = FXCollections.observableArrayList();
		observableListSelectedIngredientsToRecipe = FXCollections.observableArrayList();


		observableListAllIngredients.addAll(IngredientsList.getInstance().getIngredientsList().stream().map(e -> e.getName().toString()).collect(Collectors.toList()));

		filteredListAllIngredients = new FilteredList<>(observableListAllIngredients, p -> true);



//		for (Ingredient ingredient : IngredientsList.getInstance().getIngredientsList()) {
//
//			observableListAllIngredients.ad
//		}


	}


// Metoda skopiowana z AddIngredientModel - w tutejszej klasie prawdopodobnie zadziala poprawnie identyczna metoda
	public void refreShIingredientsObservableList() {
		observableListAllIngredients.clear();

		for (Ingredient ingredient : IngredientsList.getInstance().getIngredientsList()) {
//			AddIngredientModel.getInstance().getIngredientsObservableList().add(ingredient.getName());
			observableListAllIngredients.add(ingredient.getName());
		}
	}

// *************************** GETTERS & SETTERS


	public FilteredList<String> getFilteredListAllIngredients() {
		return filteredListAllIngredients;
	}

	public void setFilteredListAllIngredients(FilteredList<String> filteredListAllIngredients) {
		this.filteredListAllIngredients = filteredListAllIngredients;
	}

	public ObservableList<String> getObservableListAllIngredients() {
		return observableListAllIngredients;
	}

	public void setObservableListAllIngredients(ObservableList<String> observableListAllIngredients) {
		this.observableListAllIngredients = observableListAllIngredients;
	}

	public ObservableList<GridPaneForIngredient> getObservableListSelectedIngredientsToRecipe() {
		return observableListSelectedIngredientsToRecipe;
	}

	public void setObservableListSelectedIngredientsToRecipe(
			ObservableList<GridPaneForIngredient> observableListSelectedIngredientsToRecipe) {
		this.observableListSelectedIngredientsToRecipe = observableListSelectedIngredientsToRecipe;
	}

	public static void setInstance(AddIngredientsToRecipeModel instance) {
		AddIngredientsToRecipeModel.instance = instance;
	}








}
