package addingRecipesAndIngredient;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import model.ingredientsAndRecipes.Ingredient;
import model.ingredientsAndRecipes.IngredientsList;

public class AddIngredientModel {

    private boolean isAddingAllowed;
	private ObservableList<String> ingredientsObservableList;
    private FilteredList<String> filteredIngredientList;

	private static AddIngredientModel instance;

	public static AddIngredientModel getInstance() {
		if(instance == null)
			instance = new AddIngredientModel();
		return instance;
	}

	private AddIngredientModel() {	// w konstruktorze nie tworze list tej klasy gdy� klase t� zrobilem p�niej kiedy metody
		// tworz�ce te listy byly w klasie controllera i ju� tak zostalo.
		// Uwa�am, �e wzorcowe rozwi�zanie to utworzenie tych list w konstruktorze i tak te� robie tam gdzie robie to
		// od zera np w klasie AddRecipesModel
		System.out.println("Wykonuje konstruktor model");
//		ingredientsObservableList = FXCollections.observableArrayList();
//		filteredIngredientList = new FilteredList<>(ingredientsObservableList, p -> true);

	} // Konstruktor

	public void refreShIingredientsObservableList() {
		ingredientsObservableList.clear();

		for (Ingredient ingredient : IngredientsList.getInstance().getIngredientsList()) {
//			AddIngredientModel.getInstance().getIngredientsObservableList().add(ingredient.getName());
			ingredientsObservableList.add(ingredient.getName());
		}
	}


	public boolean isAddingAllowed() {
		return isAddingAllowed;
	}

	public void setAddingAllowed(boolean isAddingAllowed) {
		this.isAddingAllowed = isAddingAllowed;
	}

	public ObservableList<String> getIngredientsObservableList() {
		return ingredientsObservableList;
	}

	public void setIngredientsObservableList(ObservableList<String> ingredientsObservableList) {
		this.ingredientsObservableList = ingredientsObservableList;
	}

	public FilteredList<String> getFilteredIngredientList() {
		return filteredIngredientList;
	}

	public void setFilteredIngredientList(FilteredList<String> filteredIngredientList) {
		this.filteredIngredientList = filteredIngredientList;
	}

	public static void setInstance(AddIngredientModel instance) {
		AddIngredientModel.instance = instance;
	}




}
