package addingRecipesAndIngredient;

import java.io.IOException;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class AddIngredientsToRecipeWindow {

	private Stage stage;
	private Scene scene;
	private Parent root;

	private static AddIngredientsToRecipeWindow instance;

	public static AddIngredientsToRecipeWindow getInstance() {

		if(instance == null)
			instance = new AddIngredientsToRecipeWindow();
		return instance;
	}

	private AddIngredientsToRecipeWindow() {
		super();

		stage = new Stage();
		try {
			root = FXMLLoader.load(getClass().getResource("AddIngredientsToRecipeWindow.fxml"));
		} catch (IOException e) {	e.printStackTrace();	}


		scene = new Scene(root, 450, 600);
		stage.setScene(scene);

	}

//**************************** GETTERS & SETTERS

	public Stage getStage() {
		return stage;
	}

	public void setStage(Stage stage) {
		this.stage = stage;
	}

	public Scene getScene() {
		return scene;
	}

	public void setScene(Scene scene) {
		this.scene = scene;
	}

	public Parent getRoot() {
		return root;
	}

	public void setRoot(Parent root) {
		this.root = root;
	}

	public static void setInstance(AddIngredientsToRecipeWindow instance) {
		AddIngredientsToRecipeWindow.instance = instance;
	}





}
