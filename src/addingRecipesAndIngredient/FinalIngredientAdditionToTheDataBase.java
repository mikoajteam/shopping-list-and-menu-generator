package addingRecipesAndIngredient;

import model.ingredientsAndRecipes.IngredientsList;
import model.ingredientsAndRecipes.Measures;

public class FinalIngredientAdditionToTheDataBase {

	public static void add(String name, Measures measure) {
		System.out.println("Final add to data base");

		// dodajDoBazyDanych nowy skladnik
		IngredientsList.getInstance().addNewIngredientToDataBaseAndList(name, measure);

		//Refresh liste klasy AddIngredientModel!!!!!
		AddIngredientModel.getInstance().refreShIingredientsObservableList();
		//Refresh liste klasy AddIngredientsToResipeModel!!!!!
		AddIngredientsToRecipeModel.getInstance().refreShIingredientsObservableList();


	}

}
