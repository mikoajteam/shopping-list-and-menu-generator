package addingRecipesAndIngredient;

import java.io.IOException;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class AddRecipesWindow {

	private static AddRecipesWindow instance;

	private Stage stage;
	private Scene scene;
	private Parent root;

	public static AddRecipesWindow getInstance() {

		if (instance == null)
			instance = new AddRecipesWindow();
		return instance;
	}


	private AddRecipesWindow() {
		super();

		stage = new Stage();
		try {
			root = FXMLLoader.load(getClass().getResource("AddRecipesWindow.fxml"));
		} catch (IOException e) {		}

		scene = new Scene(root, 900, 600);
		stage.setScene(scene);

System.out.println("SINGLETON RECIPES WINDOW DONE");
	}


//*********************		GETTERY & SETTERY

	public Stage getStage() {
		return stage;
	}


	public void setStage(Stage stage) {
		this.stage = stage;
	}


	public Scene getScene() {
		return scene;
	}


	public void setScene(Scene scene) {
		this.scene = scene;
	}


	public Parent getRoot() {
		return root;
	}


	public void setRoot(Parent root) {
		this.root = root;
	}


	public static void setInstance(AddRecipesWindow instance) {
		AddRecipesWindow.instance = instance;
	}








}
