package application;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;


public class Main extends Application {
	private static Stage stage;

	@Override
	public void start(Stage primaryStage) {
		System.out.println("poczatek metody start");

		try {
			stage = primaryStage;
			Parent root = FXMLLoader.load(getClass().getResource("MainWindow.fxml"));
			root.setId("root");

			Scene scene = new Scene(root,1200,620);
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			System.out.println("start" + Controller.howManyDaysList.toString());

			stage.setScene(scene);
			stage.show();

		}  catch(Exception e) {
			e.printStackTrace();
		}
		System.out.println("koniec metody start");
	}


	public static void main(String[] args) {
		launch(args);
	}


	public static Stage getStage() {
		return stage;
	}


	public static void setStage(Stage stage) {
		Main.stage = stage;
	}


}
