package application;

import java.util.List;

import javafx.collections.ObservableList;
import javafx.geometry.HPos;
import javafx.geometry.VPos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.RowConstraints;
import javafx.stage.Stage;

public class ConfirmationWindow /*implements EventHandler<ActionEvent>*/ {

	private static ConfirmationWindow instance = null;

	private Stage stage;
	private Scene scene;
	private GridPane root;
	private	Label label;
	private Button buttonYes;
	private Button buttonNo;
	private ColumnConstraints kolumn10;
	private ColumnConstraints kolumn35;
	private RowConstraints row10;
	private RowConstraints row35;
	private List<ObservableList<Button>> listOfObservableListEachDay;
	private List<ListView<Button>> listOfListViewEachDay;


//	@Override
//	public void handle(ActionEvent event) {
//		Button buttonSource = (Button) event.getSource();
//		Scene sceneSource = buttonSource.getScene();
//
//		getInstance();
//	}

// singleton przyjmuj�cy w argumentach controlki widokowe z innego okna na kt�rych b�dzie operowa� - bardzo dobre rozwi�zanie
// cho�
// drugie i w zasadzie lepsze rozwi�zanie to U�ycie pe�nego MVC - tzn dane na kt�rych si� operuje nie wpisywa� w Controllerze tylko w klasie Model - Model najlepiej niech te� jest singletonem
	public static ConfirmationWindow getInstance(List<ObservableList<Button>> listOfObservableListEachDay, List<ListView<Button>> listOfListViewEachDay, Button buttonMakeDiagram, HBox hBoxforDiagrams) {
		if (instance == null) {
			instance = new ConfirmationWindow(listOfObservableListEachDay, listOfListViewEachDay, buttonMakeDiagram, hBoxforDiagrams);
			System.out.println("Stworzylem obiekt singletona");
		}
		return instance;
	}

	private ConfirmationWindow(List<ObservableList<Button>> listOfObservableListEachDay, List<ListView<Button>> listOfListViewEachDay, Button buttonMakeDiagram, HBox hBoxforDiagrams) {
		super();
		stage = new Stage();
		root = new GridPane();
		scene = new Scene(root, 400, 150);
		stage.setScene(scene);
		label = new Label("Czy na pewno chcesz skasowa� bie��cy grafik?");
		buttonYes = new Button("  Tak  ");
		buttonNo = new Button("  Nie  ");
		root.add(label, 1, 1, 3,1);
		root.add(buttonYes, 1, 3);
		root.add(buttonNo, 3, 3);


		kolumn10 = new ColumnConstraints();
		kolumn10.setPercentWidth(10);
		kolumn35 = new ColumnConstraints();
		kolumn35.setPercentWidth(35);
		kolumn10.setHalignment(HPos.CENTER);
		kolumn35.setHalignment(HPos.CENTER);
		root.getColumnConstraints().addAll(kolumn10, kolumn35, kolumn10, kolumn35, kolumn10);

		row10 = new RowConstraints();
		row35 = new RowConstraints();
		row10.setPercentHeight(10);
		row35.setPercentHeight(35);
		row10.setValignment(VPos.CENTER);
		row35.setValignment(VPos.CENTER);
		root.getRowConstraints().addAll(row10, row35, row10, row35, row10);

		scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
		root.getStyleClass().add("grid-layout");



		buttonNo.setOnAction((e) -> getStage().hide());

		buttonYes.setOnAction((e) -> {

		   	 System.out.println("listView size = " + listOfListViewEachDay.size());
		   	 System.out.println("observableList = " + listOfObservableListEachDay.size());

		   	hBoxforDiagrams.getChildren().removeAll(listOfListViewEachDay);
		   	Controller.setDiagramIsCreated(false);
		   	buttonMakeDiagram.setText("Stw�rz grafik");


		   	listOfListViewEachDay.clear();
		   	listOfObservableListEachDay.clear();
//			Stary kod kiedy go pisa�em nie wpadlem zeby u�yc metody clerar() :P wiec robilem to po petli
//		   	 		for (int i = listOfListViewEachDay.size() - 1; i >= 0 ; i--) {
//		   	 			System.out.println("wszedlem w for");
//							listOfListViewEachDay.remove(i);
//							listOfObservableListEachDay.remove(i);
//						}



		   	    	 System.out.println(listOfListViewEachDay.size());
		   	    	 System.out.println(listOfObservableListEachDay.size());

			getStage().hide();
		});


	}

	public Stage getStage() {
		return stage;
	}


}
