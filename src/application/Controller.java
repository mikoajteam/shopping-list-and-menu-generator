package application;

import addingRecipesAndIngredient.AddIngredientWindow;
import addingRecipesAndIngredient.AddRecipesWindow;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListView;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;

public class Controller {

	@FXML
	TabPane tabPane;

	@FXML
	ComboBox<String> comboBoxHowManyDays;

	static ObservableList<String> howManyDaysList = FXCollections.observableArrayList();

	@FXML
	ComboBox<String> comboBoxWhenStart;

	static ObservableList<String> whenStartList = FXCollections.observableArrayList();

	@FXML
	ComboBox<String> comboBoxHowManyMealsPerDay;

	static ObservableList<String> howManyMealsList = FXCollections.observableArrayList();

	@FXML
	Button buttonMakeDiagram;

	@FXML
	Button buttonReady;

	@FXML
	HBox hBoxforDiagrams;

	@FXML
	Button buttonGoToMenu;

//	RecipeList tab
	@FXML
	TextField textFieldSearchRecipe;

//	Adding tab
	@FXML
	Button buttonAddIngredient;
	@FXML
	Button buttonAddRecipe;



//	private static List<ListView<Button>> listOfListViewEachDay = new ArrayList<>();
//	private static List<ObservableList<Button>> listOfObservableListEachDay = new ArrayList<>();


	private int counter = 0;
	private static boolean diagramIsCreated = false;



	@FXML
	private void initialize() { //nadpisujemy metode initialize. metoda ta jest wykonywana przy ladowaniu nowej sceny tworzonej w sceneBuilderze??
								// W tej metodzie powinno sie dodawac elementy do kontrolek takich jak np comboBox je�li robimy widok w scene builderze
		addingElementsToHowManyDaysList(howManyDaysList);
		addingElementsToWhenStartList(whenStartList);
		addingElementsToHowManyMealsList(howManyMealsList);
		System.out.println("w metodzie initialize");

		comboBoxWhenStart.setItems(whenStartList);

		comboBoxHowManyDays.setItems(howManyDaysList);
		comboBoxHowManyDays.setPromptText("????");

		comboBoxHowManyMealsPerDay.setItems(howManyMealsList);

		System.out.println(howManyDaysList.toString());
		System.out.println("koniec metody initialize");


	}



	@FXML
	private void buttonMakeDiagramAction() {
		Integer intDays;



     if(diagramIsCreated == false) {
		if(comboBoxHowManyDays.getValue() != null && comboBoxHowManyMealsPerDay.getValue() != null) {
	    	 int naIleDniRobimygrafik = Integer.parseInt(comboBoxHowManyDays.getValue());
	    	 int ilePosilkowDziennie = Integer.parseInt(comboBoxHowManyMealsPerDay.getValue());

			System.out.println("wartosc combo ile dni == " + naIleDniRobimygrafik);
			System.out.println("wartosc combo ile posilkow == " + ilePosilkowDziennie);

			for (int i = 0; i < naIleDniRobimygrafik; i++) {
				ObservableList<Button> listaDziennychPosilkow = FXCollections.observableArrayList();
				ListView<Button> widokListyDziennychPosilkow = new ListView<>();

				widokListyDziennychPosilkow.setId("lista" + i); //narazie nie u�ywam nigdzie tego id
System.out.println(widokListyDziennychPosilkow.getId());
		        widokListyDziennychPosilkow.setItems(listaDziennychPosilkow);
		        widokListyDziennychPosilkow.setId(Integer.toString(++counter));

		        for (int j = 1; j <= ilePosilkowDziennie; j++) {

		        listaDziennychPosilkow.add(new Button("Dodaj " + j + " posi�ek"));
				}
		        ModelDiagram.getInstance().getListOfObservableListEachDay().add(listaDziennychPosilkow);
		        ModelDiagram.getInstance().getListOfListViewEachDay().add(widokListyDziennychPosilkow);
//		        listOfObservableListEachDay.add(listaDziennychPosilkow);
//		        listOfListViewEachDay.add(widokListyDziennychPosilkow);

//				hBoxforDiagrams.getChildren().add(listOfListViewEachDay.get(listOfListViewEachDay.size()-1));

		        System.out.println(ModelDiagram.getInstance().getListOfListViewEachDay().size());
		        System.out.println(ModelDiagram.getInstance().getListOfObservableListEachDay().size());
				hBoxforDiagrams.getChildren().add(ModelDiagram.getInstance().getListOfListViewEachDay().get(ModelDiagram.getInstance().getListOfListViewEachDay().size()-1));
				//hBoxforDiagrams.getChildren().add(widokListyDziennychPosilkow);

			}



			Controller.diagramIsCreated = true;
			buttonMakeDiagram.setText("Usu� bie��cy grafik");
		}
     } else {
		ConfirmationWindow.getInstance(ModelDiagram.getInstance().getListOfObservableListEachDay(), ModelDiagram.getInstance().getListOfListViewEachDay(), buttonMakeDiagram, hBoxforDiagrams)
							.getStage()
							.show();


     }


	}

	@FXML
	private void buttonGoToMenuMakerAction() {
		tabPane.getSelectionModel().select(1);

	}

	@FXML
	private void buttonAddIngredientAction() {
		AddIngredientWindow.getInstance().getStage().show();

	}

	@FXML
	private void buttonAddRecipeAction(){
		AddRecipesWindow.getInstance().getStage().show();
	}


//    public void deleteCurrentDiagram() {
//
//   	 System.out.println(listOfListViewEachDay.size());
//   	 System.out.println(listOfObservableListEachDay.size());
//   	 hBoxforDiagrams.getChildren().removeAll(listOfListViewEachDay);
//
//   	 		for (int i = listOfListViewEachDay.size() - 1; i >= 0 ; i--) {
//   	 			System.out.println("wszedlem w for");
//					listOfListViewEachDay.remove(i);
//					listOfObservableListEachDay.remove(i);
//				}
//
//
//   	    	 System.out.println(listOfListViewEachDay.size());
//   	    	 System.out.println(listOfObservableListEachDay.size());
//
//   				diagramIsCreated = false;
//   				buttonMakeDiagram.setText("Stw�rz grafik");
//    }
//
//	@FXML
//	private void buttonReadyAction() {
//		//ConfirmationWindow confirmationWindow = ConfirmationWindow.getInstance();
//	}


	private void addingElementsToWhenStartList(ObservableList<String> whenStartList) {
		whenStartList.add("Poniedzia�ek");
		whenStartList.add("Wtorek");
		whenStartList.add("�roda");
		whenStartList.add("Czwartek");
		whenStartList.add("Pi�tek");
		whenStartList.add("Sobota");
		whenStartList.add("Niedziela");

	}


	private void addingElementsToHowManyDaysList(ObservableList<String> howManyDaysList) {
		howManyDaysList.add("1");
		howManyDaysList.add("2");
		howManyDaysList.add("3");
		howManyDaysList.add("4");
		howManyDaysList.add("5");
		howManyDaysList.add("6");
		howManyDaysList.add("7");
	}

	private void addingElementsToHowManyMealsList(ObservableList<String> howManyMealsList) {
		for (int i = 1; i <= 12; i++) {
			howManyMealsList.add(Integer.toString(i));
		}
	}



	public ComboBox<String> getComboBoxHowManyDays() {
		return comboBoxHowManyDays;
	}



	public void setComboBoxHowManyDays(ComboBox<String> comboBoxHowManyDays) {
		this.comboBoxHowManyDays = comboBoxHowManyDays;
	}



	public static ObservableList<String> getHowManyDaysList() {
		return howManyDaysList;
	}



	public static void setHowManyDaysList(ObservableList<String> howManyDaysList) {
		Controller.howManyDaysList = howManyDaysList;
	}



	public ComboBox<String> getComboBoxWhenStart() {
		return comboBoxWhenStart;
	}



	public void setComboBoxWhenStart(ComboBox<String> comboBoxWhenStart) {
		this.comboBoxWhenStart = comboBoxWhenStart;
	}



	public static ObservableList<String> getWhenStartList() {
		return whenStartList;
	}



	public static void setWhenStartList(ObservableList<String> whenStartList) {
		Controller.whenStartList = whenStartList;
	}



	public ComboBox<String> getComboBoxHowManyMealsPerDay() {
		return comboBoxHowManyMealsPerDay;
	}



	public void setComboBoxHowManyMealsPerDay(ComboBox<String> comboBoxHowManyMealsPerDay) {
		this.comboBoxHowManyMealsPerDay = comboBoxHowManyMealsPerDay;
	}



	public static ObservableList<String> getHowManyMealsList() {
		return howManyMealsList;
	}



	public static void setHowManyMealsList(ObservableList<String> howManyMealsList) {
		Controller.howManyMealsList = howManyMealsList;
	}



	public Button getButtonMakeDiagram() {
		return buttonMakeDiagram;
	}



	public void setButtonMakeDiagram(Button buttonMakeDiagram) {
		this.buttonMakeDiagram = buttonMakeDiagram;
	}



	public Button getButtonReady() {
		return buttonReady;
	}



	public void setButtonReady(Button buttonReady) {
		this.buttonReady = buttonReady;
	}



	public HBox gethBoxforDiagrams() {
		return hBoxforDiagrams;
	}



	public void sethBoxforDiagrams(HBox hBoxforDiagrams) {
		this.hBoxforDiagrams = hBoxforDiagrams;
	}



	public int getCounter() {
		return counter;
	}



	public void setCounter(int counter) {
		this.counter = counter;
	}



	public static boolean isDiagramIsCreated() {
		return diagramIsCreated;
	}



	public static void setDiagramIsCreated(boolean diagramIsCreated) {
		Controller.diagramIsCreated = diagramIsCreated;
	}






}
