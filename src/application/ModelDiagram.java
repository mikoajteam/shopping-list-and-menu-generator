package application;

import java.util.ArrayList;
import java.util.List;

import javafx.collections.ObservableList;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;

public class ModelDiagram {

	private static ModelDiagram instance = null;

	private List<ListView<Button>> listOfListViewEachDay;
	private List<ObservableList<Button>> listOfObservableListEachDay;


	public static ModelDiagram getInstance() {
		if (instance == null) {
			instance = new ModelDiagram();
			System.out.println("Stworzylem obiekt singletonaModelu");
		}
		return instance;
	}

	private ModelDiagram() {
		listOfListViewEachDay = new ArrayList<>();
		listOfObservableListEachDay = new ArrayList<>();

	}

	public List<ListView<Button>> getListOfListViewEachDay() {
		return listOfListViewEachDay;
	}

	public void setListOfListViewEachDay(List<ListView<Button>> listOfListViewEachDay) {
		this.listOfListViewEachDay = listOfListViewEachDay;
	}

	public List<ObservableList<Button>> getListOfObservableListEachDay() {
		return listOfObservableListEachDay;
	}

	public void setListOfObservableListEachDay(List<ObservableList<Button>> listOfObservableListEachDay) {
		this.listOfObservableListEachDay = listOfObservableListEachDay;
	}

	public static void setInstance(ModelDiagram instance) {
		ModelDiagram.instance = instance;
	}



}
