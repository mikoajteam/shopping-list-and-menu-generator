package model.ingredientsAndRecipes;

public class IngredientWithAmount extends Ingredient {

	private double amountOfIngredient;


	public IngredientWithAmount(String name, Measures measureType, double amountOfIngredient) {
		super(name, measureType);
		this.amountOfIngredient = amountOfIngredient;

	}


	public double getAmountOfIngredient() {
		return amountOfIngredient;
	}


	public void setAmountOfIngredient(double amountOfIngredient) {
		this.amountOfIngredient = amountOfIngredient;
	}



}
