package model.ingredientsAndRecipes;

public class Ingredient implements Comparable<Ingredient> {

	protected String name;
	protected Measures measureType;


	public Ingredient(String name, Measures measureType) {
		super();
		this.name = name.toLowerCase();
		this.measureType = measureType;
	}



	@Override
	public String toString() {
		return "Ingredient [name=" + name + ", measureType=" + measureType + "]";
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((measureType == null) ? 0 : measureType.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}



	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Ingredient other = (Ingredient) obj;
		if (measureType != other.measureType)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}



	@Override
	public int compareTo(Ingredient ingredient) {

		return name.compareTo(ingredient.name);
	}


//**************************** GETTERY & SETTERY
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Measures getMeasureType() {
		return measureType;
	}

	public void setMeasureType(Measures measureType) {
		this.measureType = measureType;
	}


}
