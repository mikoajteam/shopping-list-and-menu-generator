package model.ingredientsAndRecipes;

public enum Measures {

	WEIGHT_IN_GRAMS, CAPACITY_IN_MILLILITERS, ITEMS;

}
