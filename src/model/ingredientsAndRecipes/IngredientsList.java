package model.ingredientsAndRecipes;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class IngredientsList {

	private static IngredientsList instance;

	private List<Ingredient> ingredientsList;

	private File file;
	private BufferedReader bufferedReader;
	private BufferedWriter bufferedWriter;


	public static IngredientsList getInstance(){
		if (instance == null)
			instance = new IngredientsList();
		//Dodac ? - za ka�dym razem bedzie mialo refreszowac liste skladnikow w tym miejscu
		return instance;
	}

	private IngredientsList() {
		super();
		ingredientsList = new ArrayList<>();
		file = new File("lista_skladnikow.txt");

		addingIngredientsToList();
	}

	public void addNewIngredientToDataBaseAndList(String newIngredientName, Measures measure) { // to do
		//to do
		ingredientsList.add(new Ingredient(newIngredientName, measure));

		try {
			bufferedWriter = new BufferedWriter(new FileWriter(file));


			for (Ingredient ingredient : ingredientsList) {
				bufferedWriter.write(ingredient.getName().toString());
				bufferedWriter.write(",");
				bufferedWriter.write(ingredient.getMeasureType().toString());
				bufferedWriter.write("\n");
				bufferedWriter.flush();

			}


		}catch (IOException e) {
			e.printStackTrace();
		}

		refreshAndSortList();
		addingIngredientsToList(); //p�niej zamieni na powy�sz� metode


	}

	private void refreshAndSortList() {
		// to do
	}


	private void addingIngredientsToList() {// Metoda na u�ytek konstruktora oraz na u�ytek metody addNewIngredientToDataBaseAndList() kt�ra musi na nowo wczytac liste po zapisaniu

		ingredientsList.clear();
		try {

			String line = null;
			String[] ingredientFields = null;

			try { bufferedReader = new BufferedReader(new FileReader(file)); } catch (FileNotFoundException e) { e.printStackTrace(); }

			while((line = bufferedReader.readLine()) != null ) {
				ingredientFields = line.split(",");

				ingredientsList.add(new Ingredient(ingredientFields[0], Measures.valueOf(ingredientFields[1]) ));
				System.out.println(line);
			}

		} catch (IOException e) {
			e.printStackTrace();	}
		finally {	try {
				bufferedReader.close();
			} catch (IOException e) {	e.printStackTrace();	}
		}

	}// koniec metody creatingIngredientList


// Gettery i Settery	****************************************************************************

	public List<Ingredient> getIngredientsList() {
		return ingredientsList;
	}

	public void setIngredientsList(List<Ingredient> ingredientList) {
		this.ingredientsList = ingredientList;
	}

	public static void setInstance(IngredientsList instance) {
		IngredientsList.instance = instance;
	}

}
